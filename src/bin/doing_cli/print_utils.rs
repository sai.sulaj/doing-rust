use colored::Colorize;
use doing_db::{constants, Error};

pub fn announcement(message: &str) -> String {
    format!(
        "{} {}",
        "=>".color(constants::BORDER_COLOR),
        message.color(constants::FILLER_COLOR)
    )
}

pub fn note_message(message: &str) -> String {
    message.color(constants::MESSAGE_COLOR).to_string()
}

pub fn id(id: i32) -> String {
    format!("[{}]", id)
        .color(constants::STATUS_COLOR)
        .bold()
        .to_string()
}

pub fn error(error: Error) -> String {
    format!("{}", error).red().bold().to_string()
}

pub fn tag(tag: &str) -> String {
    tag.color(constants::VARIABLE_COLOR).bold().to_string()
}
