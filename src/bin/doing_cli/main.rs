use colored::Colorize;
use std::fs;
use std::path;
use text_io::read;

mod arg_parser;
mod print_utils;
use arg_parser::get_args;
use doing_db::{constants, Date, DoingDb, Entry, EntryStatus, Result};

fn get_root_path() -> path::PathBuf {
    let mut root_path: path::PathBuf = dirs::home_dir().unwrap();
    root_path.push(".doing");

    if !root_path.exists() {
        fs::create_dir(&root_path).expect("Could not create root directory.");
    }

    root_path.push("data.db");

    root_path
}

fn now(db: &DoingDb) -> Result<()> {
    let now: Date = Date::now();
    println!("{}", print_utils::announcement("What's up?"));
    let message: String = read!("{}\n");

    let new_entry = Entry::new(None, EntryStatus::InProgress, now, message, None, None);
    let result = db.add_entry(&new_entry);
    if result.is_ok() {
        let new_entry = db.get_entry(None, None, None)?;
        println!("\n{}\n", print_utils::announcement("New Entry:"));
        println!("{}", new_entry);
    }

    Ok(())
}

fn list(db: &DoingDb, stub: &clap::ArgMatches) -> Result<()> {
    let status = stub.value_of("status").unwrap_or("ip");
    let status = match status {
        "all" => None,
        "ip" => Some(EntryStatus::InProgress),
        "c" => Some(EntryStatus::Complete),
        _ => panic!("Unknown entry status: {}", status),
    };
    let tags: Option<Vec<String>> = stub
        .values_of("tags")
        .map(|tags| tags.map(|t| t.to_string()).collect());
    let n: i64 = match stub.value_of("num") {
        Some(v) => v.parse::<i64>().unwrap(),
        None => 3,
    };

    let entries: Vec<Entry> = db.get_entries(None, status, tags, n)?;

    if entries.is_empty() {
        println!("No active entries");
    } else {
        for entry in entries.iter() {
            println!("{}\n{}", entry, entry.print_notes());
        }
    }

    Ok(())
}

fn done(db: &DoingDb, stub: &clap::ArgMatches) -> Result<()> {
    let id: i32 = stub.value_of("id").unwrap().parse::<i32>().unwrap();

    db.set_entry_status(id, EntryStatus::Complete)?;

    println!(
        "{} {} {}",
        "Entry".color(constants::FILLER_COLOR),
        print_utils::id(id),
        "has been marked complete.".color(constants::FILLER_COLOR),
    );

    Ok(())
}

fn amend(db: &DoingDb) -> Result<()> {
    let entry: Entry = db.get_entry(None, Some(EntryStatus::InProgress), None)?;

    println!(
        "{} {}{}\n\t{}\n\n{}",
        "Amending entry".color(constants::FILLER_COLOR),
        print_utils::id(entry.id),
        ". Previous message:".color(constants::FILLER_COLOR),
        print_utils::note_message(&entry.message),
        "Next message:".color(constants::FILLER_COLOR),
    );
    let new_message: String = read!("{}\n");
    db.amend_latest_message(&new_message)?;

    println!(
        "{} {} {}",
        "Entry".color(constants::FILLER_COLOR),
        print_utils::id(entry.id),
        "has been amended.".color(constants::FILLER_COLOR),
    );

    Ok(())
}

fn note(db: &DoingDb, stub: &clap::ArgMatches) -> Result<()> {
    let id = stub.value_of("id");

    let entry_id: i32 = if let Some(id) = id {
        id.parse::<i32>().unwrap()
    } else {
        let entry: Entry = db.get_entry(None, Some(EntryStatus::InProgress), None)?;
        entry.id
    };
    let entry = db.get_entry(Some(entry_id), None, None)?;

    println!(
        "{}\n\n{}\n{}\n",
        "Adding note to entry:".color(constants::FILLER_COLOR),
        entry,
        "New note:".color(constants::FILLER_COLOR),
    );

    let message: String = read!("{}");

    db.add_note(entry_id, &message)?;
    Ok(())
}

fn tags_add(db: &DoingDb, stub: &clap::ArgMatches) -> Result<()> {
    let tag = stub.value_of("tag").unwrap();
    let entry_id = stub.value_of("id");

    let entry_id: i32 = if let Some(id) = entry_id {
        id.parse::<i32>().unwrap()
    } else {
        let entry: Entry = db.get_entry(None, Some(EntryStatus::InProgress), None)?;
        entry.id
    };

    db.add_tag(entry_id, tag)?;

    Ok(())
}

fn tags_delete(db: &DoingDb, stub: &clap::ArgMatches) -> Result<()> {
    let tag = stub.value_of("tag").unwrap();
    let entry_id = stub.value_of("id");

    let entry_id: i32 = if let Some(id) = entry_id {
        id.parse::<i32>().unwrap()
    } else {
        let entry: Entry = db.get_entry(None, Some(EntryStatus::InProgress), None)?;
        entry.id
    };

    db.delete_tag(entry_id, tag)?;

    Ok(())
}

fn tags_list(db: &DoingDb, stub: &clap::ArgMatches) -> Result<()> {
    let search = stub.value_of("search");

    let tags = db.get_tags(search)?;

    for tag in tags {
        println!("{}", print_utils::tag(&tag));
    }

    Ok(())
}

fn tags(db: &DoingDb, stub: &clap::ArgMatches) -> Result<()> {
    match stub.subcommand() {
        ("add", Some(stub)) => tags_add(db, stub),
        ("delete", Some(stub)) => tags_delete(db, stub),
        ("list", Some(_)) => tags_list(db, stub),
        _ => tags_list(db, stub),
    }
}

fn main() {
    let db: Result<DoingDb> = DoingDb::open(&get_root_path());

    if let Err(err) = db {
        println!("{}", print_utils::error(err));
        std::process::exit(-1);
    }

    let db = db.unwrap();

    let args = get_args();
    let result = match args.subcommand() {
        ("now", Some(_)) => now(&db),
        ("done", Some(stub)) => done(&db, stub),
        ("amend", Some(_)) => amend(&db),
        ("note", Some(stub)) => note(&db, stub),
        ("tags", Some(stub)) => tags(&db, stub),
        ("list", Some(stub)) => list(&db, stub),
        _ => list(&db, &args),
    };

    match result {
        Ok(_) => std::process::exit(0),
        Err(err) => {
            println!("{}", print_utils::error(err));
            std::process::exit(-1);
        }
    };
}
