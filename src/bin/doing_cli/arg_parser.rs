use clap::{App, Arg, ArgMatches, SubCommand};

pub fn get_args<'a>() -> ArgMatches<'a> {
    App::new("Doing — Rust")
        .version(clap::crate_version!())
        .author("Saimir Sulaj <hola@saimirsulaj.com>")
        .about("Simple CLI-based task manager")
        .subcommand(
            SubCommand::with_name("tags")
                .about("Manage tags")
                .arg(
                    Arg::with_name("search")
                        .short("s")
                        .takes_value(true)
                        .help("A search string"),
                )
                .subcommand(
                    SubCommand::with_name("add")
                        .about("Adds a tag to an entry. If no id is specified, adds a tag to the most recent entry")
                        .arg(
                            Arg::with_name("tag")
                                .takes_value(true)
                                .required(true)
                                .help("The tag to add"),
                        )
                        .arg(
                            Arg::with_name("id")
                                .takes_value(true)
                                .help("The entry id to add the tag to"),
                        )
                )
                .subcommand(
                    SubCommand::with_name("delete")
                        .about("Deletes a tag from an entry. If no id is specified, deletes a tag from the most recent entry")
                        .arg(
                            Arg::with_name("tag")
                                .takes_value(true)
                                .required(true)
                                .help("The tag to delete"),
                        )
                        .arg(
                            Arg::with_name("id")
                                .takes_value(true)
                                .help("The entry id to delete the tag from"),
                        )
                ),
        )
        .subcommand(
            SubCommand::with_name("list")
                .about("Fetch latest <num> in-progress entries from the database")
                .arg(
                    Arg::with_name("status")
                        .required(true)
                        .possible_values(&["all", "ip", "c"])
                        .default_value("ip")
                        .help("The status to filter entries by"),
                )
                .arg(
                    Arg::with_name("num")
                        .required(true)
                        .default_value("3")
                        .help("The number of entries to return"),
                )
                .arg(
                    Arg::with_name("tags")
                        .takes_value(true)
                        .multiple(true)
                        .short("t")
                        .help("The tags to filter entries by"),
                ),
        )
        .subcommand(
            SubCommand::with_name("done")
                .about("Flag the entry with id <id> as complete")
                .arg(
                    Arg::with_name("id")
                        .required(true)
                        .help("The id of the entry to flag as complete"),
                ),
        )
        .subcommand(
            SubCommand::with_name("amend")
                .about("Overwrite the message of the latest inserted entries"),
        )
        .subcommand(
            SubCommand::with_name("now")
                .about("Insert a new entry to the database")
        )
        .subcommand(
            SubCommand::with_name("note")
                .about("Adds a note to an entry. Entry ID is optional, defaults to latest")
                .arg(Arg::with_name("id").help("The target entry ID")),
        )
        .get_matches()
}
