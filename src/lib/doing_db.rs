use super::date::Date;
use super::entry::Entry;
use super::entry_status::EntryStatus;
use super::note::Note;
use super::types;
use chrono::Utc;
use rusqlite::{params, Connection};
use snafu::ResultExt;
use sql_builder::{quote, SqlBuilder};
use std::{path, str};

fn init_tables(conn: &Connection) -> types::Result<()> {
    conn.execute_batch(
        "
            BEGIN;
            CREATE TABLE IF NOT EXISTS entries (
                id INTEGER PRIMARY KEY,
                status INTEGER NOT NULL DEFAULT 0,
                datetime TEXT NOT NULL,
                message TEXT NOT NULL DEFAULT \"\"
            );
            CREATE TABLE IF NOT EXISTS notes (
                id INTEGER PRIMARY KEY,
                entry_id INTEGER NOT NULL,
                datetime TEXT NOT NULL,
                message TEXT NOT NULL DEFAULT \"\",
                FOREIGN KEY(entry_id) REFERENCES entries(id)
            );
            CREATE TABLE IF NOT EXISTS tags (
                id INTEGER PRIMARY KEY,
                entry_id INTEGER NOT NULL,
                tag TEXT NOT NULL DEFAULT \"\",
                FOREIGN KEY(entry_id) REFERENCES entries(id)
                UNIQUE(entry_id, tag)
            );
            COMMIT;
        ",
    )
    .context(types::SqliteError)?;

    Ok(())
}

pub struct DoingDb {
    db: Connection,
}
impl DoingDb {
    pub fn new(conn: Connection) -> types::Result<Self> {
        init_tables(&conn)?;

        Ok(DoingDb { db: conn })
    }

    pub fn open(path: &path::Path) -> types::Result<Self> {
        let conn = Connection::open(path).context(types::SqliteError)?;

        init_tables(&conn)?;

        Ok(DoingDb { db: conn })
    }

    pub fn get_entry_notes(&self, entry_id: i32) -> types::Result<Vec<Note>> {
        if !self.exists(entry_id)? {
            return types::EntryNotFound {}.fail();
        }

        let mut stmt = self
            .db
            .prepare(
                "
                SELECT
                    id, entry_id, datetime, message
                FROM notes
                    WHERE entry_id = ?1
                ORDER BY id DESC
            ",
            )
            .context(types::SqliteError)?;

        let notes_iter = stmt
            .query_map(params![entry_id], |row| {
                Ok(Note::new(
                    Some(row.get(0)?),
                    row.get(1)?,
                    row.get(2)?,
                    row.get(3)?,
                ))
            })
            .context(types::SqliteError)?;

        notes_iter
            .map(|note| note.context(types::SqliteError))
            .collect()
    }

    pub fn get_entry_tags(&self, entry_id: i32) -> types::Result<Vec<String>> {
        if !self.exists(entry_id)? {
            return types::EntryNotFound {}.fail();
        }

        let mut stmt = self
            .db
            .prepare(
                "
                SELECT
                    tag
                FROM tags
                    WHERE entry_id = ?1
            ",
            )
            .context(types::SqliteError)?;

        let tags_iter = stmt
            .query_map(params![entry_id], |row| row.get(0))
            .context(types::SqliteError)?;

        tags_iter
            .map(|tag| tag.context(types::SqliteError))
            .collect()
    }

    pub fn get_tags(&self, search: Option<&str>) -> types::Result<Vec<String>> {
        let mut sql = SqlBuilder::select_from("tags");
        sql.distinct().field("tag").order_by("id", true);

        if let Some(tag) = search {
            sql.and_where_like("tag", format!("%{}%", tag));
        }

        let mut stmt = self
            .db
            .prepare(&sql.sql().unwrap())
            .context(types::SqliteError)?;

        let tags_iter = stmt
            .query_map(rusqlite::NO_PARAMS, |row| row.get(0))
            .context(types::SqliteError)?;

        tags_iter
            .map(|tag| tag.context(types::SqliteError))
            .collect()
    }

    pub fn add_tag(&self, entry_id: i32, tag: &str) -> types::Result<()> {
        if !self.exists(entry_id)? {
            return types::EntryNotFound {}.fail();
        }

        self.db
            .execute(
                "
                INSERT OR IGNORE INTO tags (tag, entry_id)
                VALUES (?1, ?2)
            ",
                params![tag.to_lowercase(), entry_id],
            )
            .context(types::SqliteError)?;

        Ok(())
    }

    pub fn delete_tag(&self, entry_id: i32, tag: &str) -> types::Result<()> {
        if !self.exists(entry_id)? {
            return types::EntryNotFound {}.fail();
        }

        self.db
            .execute(
                "
                    DELETE FROM tags
                    WHERE id = ?1 AND tag = ?2
                ",
                params![entry_id, tag],
            )
            .context(types::SqliteError)?;

        Ok(())
    }

    pub fn get_entry(
        &self,
        id: Option<i32>,
        status: Option<EntryStatus>,
        tags: Option<Vec<String>>,
    ) -> types::Result<Entry> {
        let mut entries = self.get_entries(id, status, tags, 1)?;

        if entries.is_empty() {
            return Err(types::EntryNotFound.build());
        }

        Ok(entries.pop().unwrap())
    }

    pub fn add_entry(&self, entry: &Entry) -> types::Result<usize> {
        self.db
            .execute(
                "
                INSERT INTO entries (status, datetime, message)
                VALUES (?1, ?2, ?3)
            ",
                params![entry.status, entry.datetime, entry.message],
            )
            .context(types::SqliteError)
    }

    pub fn get_entries(
        &self,
        id: Option<i32>,
        status: Option<EntryStatus>,
        tags: Option<Vec<String>>,
        limit: i64,
    ) -> types::Result<Vec<Entry>> {
        let mut sql = SqlBuilder::select_from("entries");
        sql.fields(&[
            "entries.id",
            "entries.status",
            "entries.datetime",
            "entries.message",
        ]);

        if let Some(tags) = tags {
            let tags: Vec<String> = tags.into_iter().map(quote).collect();

            sql.left()
                .join("tags")
                .on("entries.id = tags.entry_id")
                .and_where_in("tags.tag", &tags);
        }

        if let Some(id) = id {
            sql.and_where_eq("entries.id", id);
        }

        if let Some(status) = status {
            sql.and_where_eq("entries.status", status as i64);
        }

        sql.order_by("entries.id", true);
        sql.limit(limit);

        let sql = sql.sql().unwrap();

        let mut stmt = self.db.prepare(&sql).context(types::SqliteError)?;

        let entry_iter = stmt
            .query_map(rusqlite::NO_PARAMS, |row| {
                Ok(Entry::new(
                    Some(row.get(0)?),
                    row.get(1)?,
                    row.get(2)?,
                    row.get(3)?,
                    None,
                    None,
                ))
            })
            .context(types::SqliteError)?;

        entry_iter
            .map(|entry| {
                let mut entry = entry.context(types::SqliteError)?;
                let notes = self.get_entry_notes(entry.id)?;
                entry.add_notes(&notes);
                let tags = self.get_entry_tags(entry.id)?;
                entry.add_tags(&tags);
                Ok(entry)
            })
            .collect()
    }

    pub fn amend_latest_message(&self, message: &str) -> types::Result<usize> {
        self.db
            .execute(
                "
                UPDATE entries
                SET
                    message = ?1
                ORDER BY id DESC
                LIMIT 1
            ",
                params![message],
            )
            .context(types::SqliteError)
    }

    pub fn set_entry_status(&self, id: i32, status: EntryStatus) -> types::Result<usize> {
        if !self.exists(id)? {
            return types::EntryNotFound {}.fail();
        }

        self.db
            .execute(
                "
                UPDATE entries
                SET
                    status = ?1
                WHERE
                    id = ?2
            ",
                params![status, id],
            )
            .context(types::SqliteError)
    }

    pub fn exists(&self, id: i32) -> types::Result<bool> {
        let mut stmt = self
            .db
            .prepare(
                "
                    SELECT
                        COUNT(id)
                    FROM entries
                        WHERE id = ?1
                ",
            )
            .context(types::SqliteError)?;

        let mut count_iter = stmt
            .query_map(params![id], |row| row.get::<usize, i64>(0).map(|c| c > 0))
            .context(types::SqliteError)?;

        count_iter
            .next()
            .unwrap_or(Ok(false))
            .context(types::SqliteError)
    }

    pub fn add_note(&self, entry_id: i32, message: &str) -> types::Result<usize> {
        if !self.exists(entry_id)? {
            return types::EntryNotFound {}.fail();
        }

        let now: Date = Date(Utc::now());

        self.db
            .execute(
                "
                INSERT INTO notes(entry_id, datetime, message)
                VALUES (?1, ?2, ?3)
            ",
                params![entry_id, now, message],
            )
            .context(types::SqliteError)
    }
}
