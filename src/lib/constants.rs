use colored::Color;

pub const WHITE: Color = Color::TrueColor {
    r: 255,
    g: 255,
    b: 255,
};
pub const BLUE: Color = Color::TrueColor {
    r: 36,
    g: 121,
    b: 208,
};

pub const MESSAGE_COLOR: Color = WHITE;
pub const VARIABLE_COLOR: Color = BLUE;
pub const STATUS_COLOR: Color = BLUE;
pub const BORDER_COLOR: Color = BLUE;
pub const FILLER_COLOR: Color = WHITE;
