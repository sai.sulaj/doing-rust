use super::constants;
use chrono::{DateTime, Local, Utc};
use colored::Colorize;
use rusqlite::{types, ToSql};
use std::cmp::PartialEq;
use std::fmt;

#[derive(Clone, Copy, PartialEq)]
pub struct Date(pub DateTime<Utc>);
impl Date {
    pub fn now() -> Self {
        Date(Utc::now())
    }

    pub fn get_date(
        &self,
    ) -> chrono::format::DelayedFormat<chrono::format::strftime::StrftimeItems> {
        self.0.with_timezone(&Local).format("%B %e, %Y")
    }

    pub fn get_time(
        &self,
    ) -> chrono::format::DelayedFormat<chrono::format::strftime::StrftimeItems> {
        self.0.with_timezone(&Local).format("%H:%M")
    }

    pub fn from_now(&self) -> String {
        let now: DateTime<Utc> = Utc::now();
        let diff: chrono::Duration = now.signed_duration_since(self.0);

        let minutes: i64 = diff.num_minutes();

        if minutes < 60 {
            return format!("{} mins ago", minutes);
        } else if minutes < 1_440 {
            return format!("{} hours ago", diff.num_hours());
        } else if minutes < 10_080 {
            return format!("{} days ago", diff.num_days());
        } else {
            return format!("{} wks ago", diff.num_weeks());
        }
    }

    pub fn diff_str(&self) -> String {
        format!(
            "{}",
            format!("({})", self.from_now()).color(constants::VARIABLE_COLOR)
        )
    }
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let d = self
            .0
            .with_timezone(&Local)
            .format("%B%e, %Y")
            .to_string()
            .color(constants::VARIABLE_COLOR)
            .underline()
            .bold();
        let t = self
            .0
            .with_timezone(&Local)
            .format("%H:%M")
            .to_string()
            .color(constants::VARIABLE_COLOR)
            .underline()
            .bold();
        let s = format!("{} at {}", d, t).color(constants::FILLER_COLOR);
        write!(f, "{}", s)
    }
}

impl ToSql for Date {
    fn to_sql(&self) -> rusqlite::Result<types::ToSqlOutput> {
        Ok(types::ToSqlOutput::Owned(types::Value::Text(
            self.0.to_rfc3339(),
        )))
    }
}
impl types::FromSql for Date {
    fn column_result(value: types::ValueRef) -> types::FromSqlResult<Self> {
        let s = value.as_str()?;
        Ok(Date(
            DateTime::parse_from_rfc3339(s).unwrap().with_timezone(&Utc),
        ))
    }
}
