use super::constants;
use super::date::Date;
use colored::Colorize;

#[derive(Clone)]
pub struct Note {
    pub id: i32,
    pub entry_id: i32,
    pub datetime: Date,
    pub message: String,
}
impl Note {
    pub fn new(id: Option<i32>, entry_id: i32, datetime: Date, message: String) -> Self {
        Note {
            id: id.unwrap_or(0),
            entry_id,
            datetime,
            message,
        }
    }

    pub fn print_date(&self, with_diff: bool) -> String {
        let diff_str = if with_diff {
            format!(
                " {}",
                format!("({})", self.datetime.from_now()).color(constants::VARIABLE_COLOR),
            )
        } else {
            "".to_owned()
        };
        format!(
            "{} {} {} {}{}",
            "on".color(constants::FILLER_COLOR),
            format!("{}", self.datetime.get_date())
                .color(constants::VARIABLE_COLOR)
                .underline()
                .bold(),
            "at".color(constants::FILLER_COLOR),
            format!("{}:", self.datetime.get_time())
                .color(constants::VARIABLE_COLOR)
                .underline()
                .bold(),
            diff_str,
        )
    }
}
