use super::constants;
use super::date::Date;
use super::entry_status::EntryStatus;
use super::note::Note;
use colored::Colorize;
use std::fmt;

pub struct Entry {
    pub id: i32,
    pub status: EntryStatus,
    pub datetime: Date,
    pub message: String,
    pub notes: Vec<Note>,
    pub tags: Vec<String>,
}
impl Entry {
    pub fn new(
        id: Option<i32>,
        status: EntryStatus,
        datetime: Date,
        message: String,
        notes: Option<Vec<Note>>,
        tags: Option<Vec<String>>,
    ) -> Self {
        let notes = notes.unwrap_or_else(Vec::new);
        let tags = tags.unwrap_or_else(Vec::new);
        Entry {
            id: id.unwrap_or(0),
            status,
            datetime,
            message,
            notes,
            tags,
        }
    }

    pub fn add_notes(&mut self, notes: &[Note]) {
        self.notes.extend(notes.iter().cloned());
    }

    pub fn add_tags(&mut self, tags: &[String]) {
        self.tags.extend(tags.iter().cloned());
    }

    pub fn print_notes(&self) -> String {
        let mut final_str: String = String::new();

        for note in self.notes.iter().take(self.notes.len().saturating_sub(1)) {
            final_str.push_str(&format!(
                "{} {} {}{}\n{} {}\n",
                "│ ┌".color(constants::BORDER_COLOR),
                "Note added".color(constants::FILLER_COLOR),
                format!("{} {}", note.datetime, note.datetime.diff_str()),
                ":".color(constants::FILLER_COLOR),
                "│ └".color(constants::BORDER_COLOR),
                note.message.color(constants::MESSAGE_COLOR)
            ));
        }
        if let Some(note) = self.notes.last() {
            final_str.push_str(&format!(
                "{} {} {}{}\n{} {}\n",
                "│ ┌".color(constants::BORDER_COLOR),
                "Note added".color(constants::FILLER_COLOR),
                format!("{} {}", note.datetime, note.datetime.diff_str()),
                ":".color(constants::FILLER_COLOR),
                "└ └".color(constants::BORDER_COLOR),
                note.message.color(constants::MESSAGE_COLOR)
            ));
        }

        final_str
    }

    pub fn print_status(&self) -> String {
        format!(
            "{} {}",
            "Status:".color(constants::FILLER_COLOR),
            self.status,
        )
    }

    pub fn print_id(&self) -> String {
        format!(
            "{}",
            format!("[{}]", self.id)
                .color(constants::STATUS_COLOR)
                .bold(),
        )
    }

    pub fn print_tags(&self) -> String {
        let tags = self
            .tags
            .iter()
            .map(|t| t.color(constants::VARIABLE_COLOR).bold().to_string())
            .collect::<Vec<String>>()
            .join(", ");

        format!("{} {}", "Tags:".color(constants::FILLER_COLOR), tags)
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let has_notes = !self.notes.is_empty();
        let last_border_char = if has_notes { "|" } else { "└" };

        write!(
            f,
            "{} {} {}\n{} {}\n{} {}\n{} {}",
            "┌".color(constants::BORDER_COLOR),
            self.print_id(),
            format!("{} {}", self.datetime, self.datetime.diff_str()),
            "│".color(constants::BORDER_COLOR),
            self.print_status(),
            "│".color(constants::BORDER_COLOR),
            self.print_tags(),
            last_border_char.color(constants::BORDER_COLOR),
            self.message.color(constants::MESSAGE_COLOR)
        )
    }
}
