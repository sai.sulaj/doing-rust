use snafu::Snafu;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum Error {
    #[snafu(display("Sqlite error: {}", source))]
    SqliteError { source: rusqlite::Error },

    #[snafu(display("Entry not found"))]
    EntryNotFound,
}

pub type Result<A = (), E = Error> = std::result::Result<A, E>;
