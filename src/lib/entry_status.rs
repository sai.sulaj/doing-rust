use super::constants;
use colored::Colorize;
use num_enum::TryFromPrimitive;
use rusqlite::{types, ToSql};
use std::{cmp::PartialEq, convert::TryFrom, fmt, str};

#[derive(TryFromPrimitive, Clone, Copy, PartialEq)]
#[repr(i64)]
pub enum EntryStatus {
    InProgress = 0,
    Complete = 1,
}
impl ToSql for EntryStatus {
    fn to_sql(&self) -> rusqlite::Result<types::ToSqlOutput> {
        Ok(types::ToSqlOutput::Owned(types::Value::Integer(
            *self as i64,
        )))
    }
}
impl types::FromSql for EntryStatus {
    fn column_result(value: types::ValueRef) -> types::FromSqlResult<Self> {
        Ok(EntryStatus::try_from(value.as_i64().unwrap()).unwrap())
    }
}
impl fmt::Display for EntryStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let status: &str = match self {
            EntryStatus::InProgress => "In Progress",
            EntryStatus::Complete => "Complete",
        };
        write!(f, "{}", status.color(constants::STATUS_COLOR).bold())
    }
}
