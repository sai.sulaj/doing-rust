use ::doing_db::{Date, DoingDb, Entry, EntryStatus};
use rusqlite::Connection;
use std::{assert, assert_eq};

fn init_db() -> DoingDb {
    let conn = Connection::open_in_memory().unwrap();
    DoingDb::new(conn).unwrap()
}

#[test]
fn write_entry() {
    let db = init_db();

    let entry = Entry::new(
        None,
        EntryStatus::InProgress,
        Date::now(),
        "<test message>".to_string(),
        None,
        None,
    );

    db.add_entry(&entry).unwrap();
}

#[test]
fn read_entry() {
    let db = init_db();

    let message = "<test message>".to_string();
    let entry_status = EntryStatus::InProgress;
    let date = Date::now();

    let entry = Entry::new(None, entry_status, date, message.clone(), None, None);
    db.add_entry(&entry).unwrap();
    let entry = db.get_entry(None, None).unwrap();

    assert_eq!(entry.message, message);
    assert!(entry.status == entry_status);
    assert!(entry.datetime == date);
}

#[test]
fn write_note() {
    let db = init_db();

    let entry = Entry::new(
        None,
        EntryStatus::InProgress,
        Date::now(),
        "<test message>".to_string(),
        None,
        None,
    );

    db.add_entry(&entry).unwrap();
    let entry = db.get_entry(None, None).unwrap();

    let entry_id = entry.id;
    let message = "<test message>".to_string();

    db.add_note(entry_id, &message).unwrap();
}

#[test]
fn read_note() {
    let db = init_db();

    let entry = Entry::new(
        None,
        EntryStatus::InProgress,
        Date::now(),
        "<test message>".to_string(),
        None,
        None,
    );

    db.add_entry(&entry).unwrap();
    let entry = db.get_entry(None, None).unwrap();

    let entry_id = entry.id;
    let message = "<test message>".to_string();

    db.add_note(entry_id, &message).unwrap();

    let note = db.get_entry_notes(entry_id).unwrap().pop();

    assert!(note.is_some());

    let note = note.unwrap();

    assert_eq!(note.entry_id, entry_id);
    assert_eq!(note.message, message);
}
