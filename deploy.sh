cargo build --release

if [[ -f "/usr/local/bin/doing" ]]; then
  rm /usr/local/bin/doing;
fi

mv target/release/doing /usr/local/bin
